class User:
    def __init__(self, first_name, last_name):
        self.__firstName = first_name
        self.__lastName = last_name

    def __str__(self):
        return f"User [firstName = {self.__firstName}; lastName = {self.__lastName}]"


if __name__ == "__main__":
    test_user = User("test", "test")
    print(test_user)
